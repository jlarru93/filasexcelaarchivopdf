<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use ntwindia\src\NTWIndia;
use phpDocumentor\Reflection\Types\Array_;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ($this->input->server('REQUEST_METHOD') == 'GET'){
			$variable['title']='First Project';
			$this->load->view('welcome_message',$variable);
		}if ($this->input->server('REQUEST_METHOD') == 'POST'){
			set_time_limit(1000);
			register_shutdown_function(array($this, 'crear_zip'));
			{
				array_map('unlink', glob("make/*"));
			}			
			$this->crear_zip($_FILES);			
		}		
	}
	public function getColums($excelColumns){
		$arr=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
		$columsNAme=array("order id","customer name","address 1","address 2","zip code","city","country","currency","exchange rate",
						"value of product","Invoice No,","Invoice Date","product name","hsn code","qty","pcs");
		$columOrder=array();
		foreach($arr as $key=>$value){
			foreach($columsNAme as $keyName=>$valueName){
			//	echo $value."/".$valueName;
				if(trim($excelColumns->getCell($value."1")->getValue())==$valueName){
					$columOrder[$valueName]=$value;
				}
			}	
		}
		return $columOrder;
	}
	public function excel(){
		if ($this->input->server('REQUEST_METHOD') == 'GET'){
			$variable['title']='Invoice creator';
			$this->load->view('welcome_message',$variable);
		}if ($this->input->server('REQUEST_METHOD') == 'POST'){
			ini_set('memory_limit', '5224M');
			set_time_limit(10000);
			{
				array_map('unlink', glob("makeArtGraf/*"));
			}	
			$excel = IOFactory::load($_FILES['fileToUpload']['tmp_name']);

			$conditional=isset($_POST['booster']);
				$excel->setActiveSheetIndex(0);
				$i=2;
				$j=0;
				$colums=$this->getColums($excel->getActiveSheet());		
				while($excel->getActiveSheet()->getCell('A'.$i)->getValue()!=""){
					$orderid=$excel->getActiveSheet()->getCell($colums['order id'].$i)->getValue();
					$customername=$excel->getActiveSheet()->getCell($colums['customer name'].$i)->getValue();
					$address1=$excel->getActiveSheet()->getCell($colums['address 1'].$i)->getValue();
					$address2=$excel->getActiveSheet()->getCell($colums['address 2'].$i)->getValue();
					$zipcode=$excel->getActiveSheet()->getCell($colums['zip code'].$i)->getValue();
					$city=$excel->getActiveSheet()->getCell($colums['city'].$i)->getValue();
					$country=$excel->getActiveSheet()->getCell($colums['country'].$i)->getValue();
					$currency=$excel->getActiveSheet()->getCell($colums['currency'].$i)->getValue();
					$exchangerate=$excel->getActiveSheet()->getCell($colums['exchange rate'].$i)->getValue();
					$valueofproduct=$excel->getActiveSheet()->getCell($colums['value of product'].$i)->getValue();
					$InvoiceNo=$excel->getActiveSheet()->getCell($colums['Invoice No,'].$i)->getValue();
					$InvoiceDate=$excel->getActiveSheet()->getCell($colums['Invoice Date'].$i)->getValue();
					$productName=$excel->getActiveSheet()->getCell($colums['product name'].$i)->getValue();
					$hsnCode=$excel->getActiveSheet()->getCell($colums['hsn code'].$i)->getValue();
					$pcs=$excel->getActiveSheet()->getCell($colums['pcs'].$i)->getValue();
					$spreadsheet=null;
					$worksheet=null;
					$writer=null;
					$spreadsheet = IOFactory::load("modelo_excel.xlsx");
					$worksheet = $spreadsheet->getActiveSheet();

					$valueCurrencyTaxAmount=$pcs*$valueofproduct*$exchangerate;
					$worksheet->setCellValue('D22', $country);
					$worksheet->setCellValue('A15', $customername."\n ".$address1."/".$address2."\n ".$zipcode."\n ".$city." ".$country);
					$worksheet->setCellValue('I14', $InvoiceNo);
					$worksheet->setCellValue('I15', substr($InvoiceDate,0,10));
					$worksheet->setCellValue('I16', $orderid);
					$worksheet->setCellValue('B25', $productName);
					$worksheet->setCellValue('D25', $hsnCode);
					$worksheet->setCellValue('E25', $pcs);
					$worksheet->setCellValue('G25', $valueofproduct);
					$worksheet->setCellValue('H25', $pcs*$valueofproduct);

					$worksheet->setCellValue('I25', $exchangerate);
					$worksheet->setCellValue('J25', $valueCurrencyTaxAmount);

					$ntw = new \NTWIndia\NTWIndia();
					$currencyWordTaxAmount=$ntw->numToWord( $valueCurrencyTaxAmount);

					$worksheet->setCellValue('A34', "NIL - SUPPLY MEANT FOR EXPORT - LETTER OF UNDERTAKING Dt 20th April, 2018 - WITHOUT PAYMENT OF INTEGRATED GOODS AND SERVICE TAX");
					$worksheet->setCellValue('A31', $currencyWordTaxAmount);

					$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
					$writer->save('makeArtGraf/'.$InvoiceNo.'.xlsx');
					if($conditional!=1){
						gc_collect_cycles();
					}
				
					$i++;
					$j++;
				}

				$filename='invoiceCreator.zip';
				$zip = new ZipArchive;
				$zip->open($filename, ZipArchive::CREATE);
				foreach (glob("makeArtGraf/*") as $file) {
					$zip->addFile($file);
					//if ($file != 'makeArtGraf/important.txt') unlink($file);
				}
				$zip->close();
				header('Content-type: application/zip');
				header('Content-Disposition: attachment; filename="invoiceCreator.zip"');
				readfile($filename);
				{
					array_map('unlink', glob("makeArtGraf/*"));
					array_map('unlink', glob("invoiceCreator.zip"));
				}	
		
		}
		
	}

	public function teste(){
		/*$editFile = "modelo_excel.xlsx";
		$sSheet = \PhpOffice\PhpSpreadsheet\IOFactory::load( $editFile );
		//working on something with the spreadsheet/worksheet
		$worksheet = $sSheet->getSheetByName('SHEET NAME');
		$spreadsheet->getActiveSheet()->setCellValueByColumnAndRow("A", "1", "helloworld");
		//this is how to write directly using loaded spreadsheet data
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($sSheet);
		$writer->save( $editFile );*/
	}

	private function crear_zip($file_arch){
		$num=0;
			$this->load->library('LibTcpdf');			
			if (isset($file_arch['fileToUpload'])) {
				$zip = new ZipArchive();
				$filename = "make/reports.zip";
				$zip->open($filename, ZipArchive::CREATE);
				$this->load->library('SimpleXLSX');
				
				if ( $xlsx = SimpleXLSX::parse( $file_arch['fileToUpload']['tmp_name'] ) ) {
					
					$dim = $xlsx->dimension();
					$cols = $dim[0];
					foreach( $xlsx->rows() as $k => $r ) {
						if($num!=0){
							$this->crear_pdf($r,$k);
							$zip->addFile('make/report_' . $k . '.pdf','report_' .$k . '.pdf');
						}
						$num++;						
					}
					$zip->close();
					header('Content-type: application/zip');
					header('Content-Disposition: attachment; filename="certificates.zip"');
					readfile($filename);
				} else {
					echo SimpleXLSX::parseError();
				}
			}

			{
				array_map('unlink', glob("make/*"));
			}
	}


	private function crear_pdf($r,$k){
		$orderId=$r[0];
		$productName=$r[1];
		$itemPrice=$r[2];
		$recipientName=$r[3];
		$shipAddress1=$r[4];
		$shipAddress2=$r[5];
		$shipAddress3=$r[6];
		$shipCity=$r[7];
		$shipPostalCode=$r[8];
		$shipCountry=$r[9];
		$TRACKINGID=$r[10];
		$WEIGHT=$r[11];
		$completeAddressSender="M/s arts and crafts, house no.82, street no.9, Balbir nagar shahdara, Delhi-110032 ohri597@gmail.com Contact no. : 8882082717";
		$bookingOffice="Foreign Post, New Delhi-110002";
		$postagepaid="";
		$completeAddressAndClient=$recipientName.";<br>".$shipAddress1."<b>/</b>".$shipAddress2."<b>/</b>".$shipAddress3."<br>"."<b>".$shipCountry."<br>".$shipCity."<br>".$shipPostalCode."</b>";
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nicola Asuni');
		$pdf->SetTitle('TCPDF Example 001');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('dejavusans', '', 14, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();

		// set text shadow effect
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

		// Set some content to print
		$html = '<!DOCTYPE html>
				<html lang="en">
				<head>
				<style>
				table { 
					border-collapse: separate;
					border-spacing: 2px;
		 			}
				</style>
				</head>
				<body>
				<H1 align="center">
					<em><u>Department of Posts India <br> Foreign Post, New Delhi-110002</u></em>
				</H1>
				<table border="1">
				<tr><td width="8%">1.<br></td><td width="46%">Receipt No. of Article <br> </td><td width="46%">'.$TRACKINGID.' <br> </td></tr>
				<tr><td width="8%">2.<br></td><td width="46%">Office of Booking with date of Booking <br> </td><td width="46%">'.$bookingOffice.'</td></tr>
				<tr><td width="8%">3.<br></td><td width="46%">Complete address of the Addressee with name where the Letter is posted <br> </td><td width="46%">'.$completeAddressAndClient.' <br> </td></tr>
				<tr><td width="8%">4.<br></td><td width="46%">Complete address of the sender with name Contact no. and e-mail id for reply <br> </td><td width="46%">'.$completeAddressSender.' <br> </td></tr>
				<tr><td width="8%">5.<br></td><td width="46%">Weight of the article <br> </td><td width="46%">'.$WEIGHT.' <br> </td></tr>
				<tr><td width="8%">6.<br></td><td width="46%">Content details inside the Article <br> </td><td width="46%">'.$productName.' <br> </td></tr>
				<tr><td width="8%">7.<br></td><td width="46%">Postage paid for the article <br> </td><td width="46%">'.$postagepaid.' <br> </td></tr>
				<tr><td width="8%">8.<br></td><td width="46%">Value of the contents <br> </td><td width="46%">'.$itemPrice.' <br> </td></tr>
				<tr><td width="8%">9.<br></td><td width="46%">Signature of the sender <br> </td><td width="46%">'."".' <br> </td></tr>
					
				</table>
				</body>
				</html>';

		// Print text using writeHTMLCell()
		$pdf->writeHTML($html, true, false , true, false, "L");
		$pdf->lastPage();
		$pdf->Output('make/report_' . $k . '.pdf', 'F');
		$pdf->Close();
			// ---------------------------------------------------------
	}
}
