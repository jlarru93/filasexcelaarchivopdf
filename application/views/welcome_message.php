<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<script
  src="https://code.jquery.com/jquery-3.4.1.slim.js"
  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
  crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

</head>
<body>
<div class="bd-example">
<a href="./index" class="btn btn-primary btn-lg <?php echo $title=='Invoice creator'?'':'active'; ?>" role="button" aria-pressed="true">Firts project</a>
<a href="./excel" class="btn btn-primary btn-lg <?php echo $title=='Invoice creator'?'active':''; ?>" role="button" aria-pressed="true">Invoice creator</a>
</div>
<br>

<h1><?php echo $title;?></h1>
	<div class="custom-file w-25 p-3">
		<form enctype="multipart/form-data" method="post">
			<input type="file" name="fileToUpload" id="fileToUpload" 
			class="custom-file-input"
			accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
			 required>
			<label class="custom-file-label" for="fileToUpload" >Choose file</label>
			<?php if($title=='Invoice creator'?true:false){?>
				<div class="custom-control custom-checkbox">
					<input type="checkbox" class="custom-control-input" name="booster" value="Yes" id="customCheck1">
					<label class="custom-control-label" for="customCheck1">
						booster
					</label>
				</div>
			<?php }?>
			<br>
			<input type="submit" value="Upload excel and download zip pdf file" name="submit" class="btn btn-primary">
		</form>
	<div>
	<script>
            $('#fileToUpload').on('change',function(){
                //get the file name
                var fileName = $(this).val();
				console.log(fileName);
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>
</body>
</html>